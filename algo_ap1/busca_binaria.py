# busca em uma lsta ordenada

def pesquisa_binaria(a, item):
    """Implementa pesquisa binária iterativamente."""
    esquerda = 0
    direita = len(a) - 1

    while esquerda <= direita:
        meio = (esquerda + direita) // 2

        if a[meio] == item:
            return meio

        elif a[meio] > item:
            direita = meio - 1

        else: # a[meio] < item
            esquerda = meio + 1
    
    return -1

a=[1,2,3,4,5,6,7,8,9]

item=89

print(pesquisa_binaria(a,item))