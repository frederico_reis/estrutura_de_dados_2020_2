# # Recursive Python function to solve the tower of hanoi 

# def TowerOfHanoi(n , source, destination, auxiliary): 
# 	if n==1: 
# 		print ("Move disk 1 from source",source,"to destination",destination) 
# 		return
# 	TowerOfHanoi(n-1, source, auxiliary, destination) 
# 	print ("Move disk",n,"from source",source,"to destination",destination) 
# 	TowerOfHanoi(n-1, auxiliary, destination, source) 
		
# # Driver code 
# n = 6
# TowerOfHanoi(n,'A','B','C') 
# # A, C, B are the name of rods 

# # Contributed By Dilip Jain 


#iterative solution
def tower(numRings):
    result = []
    stk = []
    def tower1(numRings):
        In, To = 0, 1
        stk.append((In,numRings,0,1,2))
        while stk:
            state,numRings,fromPeg,toPeg,usePeg = stk.pop()
            if state == In:
                if numRings != 0: #push down to 1 numRing #A
                    stk.append((To,numRings,fromPeg,toPeg,usePeg)) #B save state
                    stk.append((In,numRings-1,fromPeg,usePeg,toPeg)) 
            elif state == To:
                result.append((fromPeg,toPeg)) #C
                stk.append((In,numRings-1,usePeg,toPeg,fromPeg)) #D
            else:
                print ('Error: logic')
                return result
    tower1(numRings)
    return result

a = [1,2,3,4]
for n in a:
    print ('rings',n)
    print (tower(n))
